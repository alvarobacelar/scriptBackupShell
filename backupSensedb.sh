#!/bin/bash

# SCRIPT DE BACKUP DO BANCO DE DADOS DO SENSE
# DATA CRIAÇÃO: 12/09/16
# ÁLVARO BACELAR
# EMAIL: alvaro@infoway-pi.com.br

#======= Variaveis do script ========#
USERDB="backup"
HOSTDB="localhost"
ANO="$(date +"%Y")"
MES="$(date +"%m")"

DESTBKP="/home/backupSense/dumps"
LOGDIR="/home/backupSense/dumpLog.log"
NOW="$(date +"%Y%m%d-%H%M")"
DBBKP=$*
PATHS3="s3://senseinfoway/uniplam/$ANO/$MES/"

realizarBackup(){

	echo "----------------------------------- SCRIPT DE BACKUP DO SENSE ------------------------------------------" >> $LOGDIR
	echo "$(date) - Verificando se existe a pasta para por o backup" >> $LOGDIR	 
	if [ ! -e "" ]; then
	   mkdir -p $DESTBKP
	   echo "$(date) - Diretorio $DESTBKP não existende, criando a pasta para por os arquivos de dump" >> $LOGDIR
	fi
	
	echo "$(date) - Verificando se foi informando qual banco realizar backup" >> $LOGDIR
	if [ -z $DBBKP ]; then
	   echo "$(date) - Erro ao executar o script, não foi informado qual banco realizar backup" >> $LOGDIR
	   exit 1
	fi
	
	CONECTADB="$(mysql -h $HOSTDB -u $USERDB -Bse 'SHOW DATABASES')"
	if [ $? -ne 0 ]; then	 
	   echo "$(date) - Erro ao se conectar com o banco de dados" >> $LOGDIR
	   exit 1
	fi
	
	cd $DESTBKP
	for db in $DBBKP 
	do 	   
	   echo "" >> $LOGDIR
	   echo "---------------------------------- INICIO DO BACKUP DO $db -------------------------------------------" >> $LOGDIR 
	   FILE="Backup-$db-$NOW"
	   echo "$(date) - Realizando o dump do banco de dados $db" >> $LOGDIR
	   mysqldump -h $HOSTDB -u $USERDB  $db > $FILE.backup
	   if [ $? -ne 0 ]; then	     
	      echo "$(date) - Erro ao realizar o dump do banco de dados $db" >> $LOGDIR
	   fi
	   
	   echo "$(date) - Compactando o arquivo de backup" >> $LOGDIR
	   tar czf $FILE.tar.gz $FILE.backup
	   
	   if [ $? -ne 0 ]; then	      
	      echo "$(date) - Erro ao compactar o arquivo de backup" >> $LOGDIR
	      rm -rf $FILE.tar.gz
	   fi	   
	   
	   echo "$(date) - Removendo o arquivo de backup $FILE.backup" >> $LOGDIR
	   rm -rf $FILE.backup

	   echo "$(date) - Enviando o arquivo de backup $FILE.tar.gz para a S3" >> $LOGDIR
	   aws s3 cp $FILE.tar.gz $PATHS3

	   if [ $? -ne 0 ]; then
	      echo "$(date) - Erro ao enviar o arquivo de backup para a S3" >> $LOGDIR
	   fi	   
	   
	   echo "$(date) - Removendo o arquivo de backup $FILE já enviado para S3" >> $LOGDIR
	   rm -rvf $FILE.tar.gz
	   
	   if [ $? -ne 0 ]; then
	      echo "$(date) - Ocorreu erros ao realizar o backup do banco $db" >> $LOGDIR
	   else 
	      echo "$(date) - Backup do bando $db concluido com sucesso!" >> $LOGDIR
	   fi
	   echo "------------------------------------ FIM DO BACKUP DO $db --------------------------------------------" >> $LOGDIR
	   echo " " >> $LOGDIR
	done

}

realizarBackup
